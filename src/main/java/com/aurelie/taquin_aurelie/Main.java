/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aurelie.taquin_aurelie;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
/**
 *
 * @author stag
 */

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Hello");
        JFrame f = new JFrame();
        f.setTitle("Jeu du taquin");
        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        Damier damier = new Damier();
        p.add(damier,BorderLayout.CENTER);
        JButton startButton = new JButton("Start");
        startButton.setBackground(Color.orange);
        p.add(startButton,BorderLayout.SOUTH);
        
        f.add(p);

        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(500, 500);
        f.setVisible(true); 
    }   
}
