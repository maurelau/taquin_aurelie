package com.aurelie.taquin_aurelie;

import javax.swing.JButton;
import java.awt.*;

public class Carreau extends JButton{
    
    //private final int ligne;
    //private final int colonne;

    public Carreau(String name) { //(String name, int ligne, int colonne) 
        super(name);
        //this.ligne = ligne;
        //this.colonne = colonne;
        init();
    }

    private void init() {
        this.setBackground(Color.cyan);
    }
}
